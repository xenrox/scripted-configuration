#!/usr/bin/env bash

set -ex

function apt_install() {
    if ! which $@ > /dev/null; then
      apt-get install -qq -y $@
    fi
}

function apt_install_norecommends() {
    if ! which $@ > /dev/null; then
      apt-get install --no-install-recommends -qq -y $@
    fi
}

function setup_sshd () {
	apt_install openssh-server
	cat <<EOF > /etc/ssh/sshd_config.d/20-codeberg-default.conf
Port 22

HostKey /etc/ssh/ssh_host_rsa_key
HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key

X11Forwarding yes
PrintMotd no

AuthorizedKeysFile .ssh/authorized_keys
PermitRootLogin no

PubkeyAuthentication yes
PasswordAuthentication no
ChallengeResponseAuthentication no
KerberosAuthentication no
GSSAPIAuthentication no
UsePAM yes

# Only accept hardware-backed keys (SK)
# PubkeyAcceptedKeyTypes sk-ssh-ed25519@openssh.com,sk-ssh-ed25519-cert-v01@openssh.com,sk-ecdsa-sha2-nistp256@openssh.com,sk-ecdsa-sha2-nistp256-cert-v01@openssh.com

# Disabled, as it causes errors if the locale isn't installed
#AcceptEnv LANG LANGUAGE LC_*
EOF
	line_in_file "/etc/ssh/sshd_config" "Include /etc/ssh/sshd_config.d/*.conf"
	chmod 'u=rw,g=r,o=r' /etc/ssh/sshd_config.d/20-codeberg-default.conf
	systemctl restart sshd
}

function enable_backports () {
	BACKPORT_APTSOURCE_LINE="deb http://deb.debian.org/debian bullseye-backports main"
	if [ "$(grep "${BACKPORT_APTSOURCE_LINE}" /etc/apt/sources.list.d/backports.list)" == "" ]; then
		echo "${BACKPORT_APTSOURCE_LINE}" >> /etc/apt/sources.list.d/backports.list
	fi
	apt-get update
}

function backup_file () {
	HOST="${1}"
	FILEPATH="${2}"
	FILEPATH_FOLDER="$(dirname "${FILEPATH}")"

	mkdir -p "./hosts/${HOST}${FILEPATH_FOLDER}"
	cp -f "${FILEPATH}" "./hosts/${HOST}${FILEPATH}.$(date +"%Y-%m-%d").backup"
}

# install_file is used to copy a file from the config to the host system including the creation of a backup file
# usage: install_file "host" "/absolute/path/to/file"
# example: install_file "mariadb" "/etc/mysql/my.cnf" ;# (copies hosts/mariadb/etc/mysql/my.cnf)
# Files are placed into hosts/<host>/<absolute path>. The host is used to allow potentially sharing files between hosts and reusing them.
# Returns 1 if file actually changed
function install_file_v2 () {
	HOST="${1}"
	FILEPATH="${2}"

	if [ ! -f "${FILEPATH}" ]; then
		echo "Installing ${FILEPATH} for the first time."
		# -D creates directories if necessary
		install -D "./hosts/${HOST}${FILEPATH}" "${FILEPATH}"
		return 1
	fi
	DIFF="$(diff --new-file "./hosts/${HOST}${FILEPATH}" "${FILEPATH}" || true)"
	if [[ "${DIFF}" == "" ]]; then
		return 0
	fi
	echo "${DIFF}"
	cp -f "./hosts/${HOST}${FILEPATH}" "${FILEPATH}"
	return 1
}

function install_file () {
	install_file_v2 "${1}" "${2}" || true
}

# returns 1 if there was a change
function install_template () {
	HOST="${1}"
	FILEPATH="${2}"
	FILEPATH_FOLDER="$(dirname "${FILEPATH}")"

	if ! which envsubst; then
		apt-get install gettext-base
	fi

	mkdir -p "./hosts/tmp${FILEPATH_FOLDER}"
	envsubst < "./hosts/${HOST}${FILEPATH}" > "./hosts/tmp${FILEPATH}"
	RETURN=0; install_file_v2 "tmp" "${FILEPATH}" || RETURN=$?
	rm "./hosts/tmp${FILEPATH}"
	return $RETURN
}

function line_in_file () {
	FILENAME="${1}"
	LINE="${2}"

	if ! grep --fixed-strings --line-regexp "${LINE}" "${FILENAME}" ; then
		backup_file "tmp" "${FILENAME}"
		echo "${LINE}" >> "${FILENAME}"
	fi
}

function confirm {
	# $1: Confirmation dialog
	# return:
	#	0 = true
	#	1 = false
	read -p "$1 [y/N] " -n 1 -r
	echo
	if [[ $REPLY =~ ^[Yy]$ ]]; then
		return 0
	else
		return 1
	fi
}

