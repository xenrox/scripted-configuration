The forgejo-ci container is dedicated to the CI runs for Forgejo.org which is available at forgejo-ci.codeberg.org and is used to build Forgejo from the organisation forgejo and the associated forgejo-* organisations (used to seperate concerns and harden the pipelines) on Codeberg/forgejo.

Associated issues and discussions are at https://codeberg.org/forgejo/forgejo/issues?labels=79180

All changes to this directory are tested by running:

./hosts/forgejo-ci/tests/run.sh

from an LXC host (or an LXC privileged container) or automatically via https://woodpecker-local.forgejo.org.
