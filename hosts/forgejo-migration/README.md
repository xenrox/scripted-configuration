# How to use our migration container

Prepare:

- ensure that Forgejo won't auto-start if you enable the service
- you can start the container and manually run `systemctl stop gitea.service`

1. Create snapshots
    - join the MariaDB LXC container and open a tmux session
    - open the Ceph web UI or create a snapshot from shell
    - enter "/root/scripted-configuration.git/hosts/mariadb" in the mariadb LXC container
    - create the db snapshot by executing "./duplicate_prod_to_migration.sh"
    - create a ceph snapshot called "migration" in "data", "/production/git" (e.g. by navigating in the Web UI to filesystems, data, directories, production > git, create snapshot)
    - wait for the db snapshot to be created (it can take some time)
2. start container (`lxc-start forgejo-migration`)
3. deploy Forgejo from the build container (`make TARGET=forgejo-migration`). Ensure that you are in the correct branch and actually deploy the correct version
    - maybe ensure that you switch back to the production branch to save the next admin deploying from figuring out where you are :)
4. watch the migration logs in /data/git/log/gitea.log
5. on success, you can even interactively use the migration instance, please ask us for access (protected by reverse proxy)
6. `systemctl disable gitea` and `lxc-stop forgejo-migration`