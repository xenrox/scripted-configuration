#!/usr/bin/env bash

set -ex

source "base/users.sh"

user_setup "xenrox" "sudo"
user_setup "javor" "sudo"
