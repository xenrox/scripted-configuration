#!/usr/bin/env bash

set -ex

# limit scrubbing
ceph config set global mds_max_scrub_ops_in_progress 1
ceph config set global osd_scrub_begin_hour 1
ceph config set global osd_scrub_end_hour 9
ceph config set global osd_scrub_min_interval 172800 # 2 days
ceph config set global osd_scrub_max_interval 1209600 # 14 days
ceph config set global osd_deep_scrub_interval 2419200 # 28 days
