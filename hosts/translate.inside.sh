#!/usr/bin/env bash

set -ex

source "base/users.sh"

user_setup "bubu" "sudo"

source "base/base.sh"
setup_sshd

# do more stuff, or don't and use something else
