#!/usr/bin/env bash

set -ex
source "base/base.sh"
source "base/users.sh"

apt-get install -y dropbear-initramfs
line_in_file "/etc/dropbear/initramfs/dropbear.conf" 'DROPBEAR_OPTIONS="-I 180 -j -k -p 2222 -s -c cryptroot-unlock"'
install_file_v2 "aburayama" "/etc/dropbear/initramfs/authorized_keys" || dpkg-reconfigure dropbear-initramfs

