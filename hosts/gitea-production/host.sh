#!/usr/bin/env bash

set -ex

if [ ! -d /mnt/ceph-cluster/staging/ ]; then
	mkdir -p /mnt/ceph-cluster/staging
fi

MEM=40
CPU=15
WEIGHT=95
LXC_CUSTOM=$(cat <<EOF
lxc.mount.entry = /dev/fuse dev/fuse none bind,create=file,rw 0 0
lxc.mount.entry = /mnt/ceph-cluster/production mnt/ceph-cluster none defaults,bind,create=dir 0 0
security.nesting = true
EOF
)
