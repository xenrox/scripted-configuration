#!/usr/bin/env bash

for p in $(pgrep ^git$); do
	renice -n 5 -p $p &> /dev/null 
done
