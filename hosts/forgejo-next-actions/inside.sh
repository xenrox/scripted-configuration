#!/usr/bin/env bash

set -ex

source "base/users.sh"

# ensure git has id 1001
if [ "$(id -u "git" 2>/dev/null)" == "" ]; then
	useradd --uid 1001 git
	mkdir -p /home/git
	chown git:git /home/git -R
fi

source "base/base.sh"
setup_sshd

user_grant "root" "crystal"
user_grant "root" "dachary"
user_grant "root" "gusted"
systemctl reload sshd

apt-get install -y --no-install-recommends git
