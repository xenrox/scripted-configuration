#!/usr/bin/env bash

set -ex

MEM=16
CPU=4
LXC_CUSTOM=$(cat <<EOF
lxc.mount.entry = /mnt/btrfs mnt/search-data none defaults,bind,create=dir 0 0
security.nesting = true
EOF
)
